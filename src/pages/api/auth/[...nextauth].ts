/* eslint-disable no-param-reassign */
import NextAuth from 'next-auth'
import GithubProvider from 'next-auth/providers/github'
import CredentialsProvider from 'next-auth/providers/credentials'

const LOGIN_QUERY = `
  query SignIn($loginEmail: String, $loginPassword: String) {
    user: signIn(email: $loginEmail, password: $loginPassword) {
      id
      email
      updatedAt
      createdAt
    }
  }
`

const CREDENTIALS_PROVIDER_CONFIG: any = {
  name: 'credentials',
  credentials: {
    email: { label: 'Email', type: 'email', placeholder: 'Enter your email' },
    password: { label: 'Password', type: 'password', placeholder: 'Enter your password' },
  },
  async authorize(credentials: any) {
    const res = await fetch(process.env.NEXT_PUBLIC_GATEWAY_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query: LOGIN_QUERY,
        variables: {
          loginEmail: credentials.email,
          loginPassword: credentials.password,
        },
      }),
    })
    const { data } = await res.json()
    if (res.ok && data.user) {
      // console.log(data.user)
      return data.user
    }
    return null
  },
}

export default NextAuth({
  providers: [
    GithubProvider({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET,
    }),
    CredentialsProvider(CREDENTIALS_PROVIDER_CONFIG),
  ],
  secret: process.env.JWT_SECRET,
  session: {
    strategy: 'jwt',
    maxAge: 60 * 60 * 24 * 7,
  },
  pages: {
    signIn: '/auth/signin',
    newUser: '/auth/newuser',
  },
  debug: process.env.NODE_ENV === 'development',
  callbacks: {
    async jwt({ token, account }) {
      if (account) {
        token.id = account.providerAccountId
      }
      return token
    },
    async session({ session, token }) {
      session.id = token.id
      return session
    },
  },
})
