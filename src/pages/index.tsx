/* eslint-disable jsx-a11y/anchor-is-valid */
import type { NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'

import Footer from '../components/Footer'

const Home: NextPage = () => (
  <>
    <section>
      <div className="grid grid-cols-1 sm:grid-cols-2 py-0 px-4 min-h-screen">
        <div className="py-16 px-4 my-auto">
          <Image alt="" src="/undraw_scrum_board.svg" width="821.67627" priority height="579.00958" layout="responsive" />
        </div>
        <div className="py-16 px-4 m-auto">
          <h1 className="text-left text-black">Lorem Ipsum</h1>
          <p className="text-sm text-black font-semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum, fugiat quia.</p>
        </div>
      </div>
    </section>

    <section className="grid grid-cols-1 sm:grid-cols-2 py-0 px-4">
      <div className="py-16 px-4">
        <h1 className="text-md text-left text-black">Features</h1>
        <p className="text-left text-base text-black font-semibold py-4">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ullam esse obcaecati repellendus error? Eos doloribus libero quod repellat magnam delectus, optio consectetur minima quam omnis officiis unde veritatis dicta sed.</p>
        <Link href="/workspace">
          <a className="btn btn-pink">Get Started</a>
        </Link>
      </div>
      <div className="py-16 px-4">
        <div className="grid grid-cols-2">
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-14 w-14 bg-pink-300 rounded-lg m-2 p-1 text-pink-500 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z" />
            </svg>
            <p className="text-xs md:text-base font-semibold text-black p-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit</p>
          </div>
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-14 w-14 bg-pink-300 rounded-lg m-2 p-1 text-pink-500 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z" />
            </svg>
            <p className="text-xs md:text-base font-semibold text-black p-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit</p>
          </div>
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-14 w-14 bg-pink-300 rounded-lg m-2 p-1 text-pink-500 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z" />
            </svg>
            <p className="text-xs md:text-base font-semibold text-black p-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit</p>
          </div>
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-14 w-14 bg-pink-300 rounded-lg m-2 p-1 text-pink-500 shadow-md" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z" />
            </svg>
            <p className="text-xs md:text-base font-semibold text-black p-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Lorem ipsum dolor sit amet consectetur, adipisicing elit</p>
          </div>
        </div>
      </div>
    </section>
    <Footer />
  </>
)

export default Home
