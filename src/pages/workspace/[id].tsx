import { useEffect, useState } from 'react'
import { useQuery } from '@apollo/client'
import WORKSPACE_IDS_QUERY from '../../apollo/query/WorkspaceIDsQuery'
import WORKSPACE_BY_ID_QUERY from '../../apollo/query/workspace/WorkspaceByID'
import AddBoardButton from '../../components/workspace/sidenav/AddBoardButton'
import SettingsButton from '../../components/workspace/sidenav/SettingsButton'
import SideNavButton from '../../components/workspace/sidenav/SideNavButton'
import client from '../../apollo/client'
import KanbanBoard from '../../components/workspace/boards/KanbanBoard'
import EmptyWorkspace from '../../components/workspace/EmptyWorkspace'

export async function getStaticPaths() {
  const { data } = await client.query({ query: WORKSPACE_IDS_QUERY })

  const paths = data.workspace.map((workspace: any) => ({
    params: { id: workspace.id },
  }))

  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({ params }: any) {
  const { id } = params
  // resetServerContext()
  return { props: { id } }
}

function RenderBoard({ board }: any) {
  switch (board.__typename) {
    case 'Kanban':
      return <KanbanBoard board={board} />
    default:
      return <EmptyWorkspace />
  }
}

export default function Workspaces({ id }: any) {
  const { loading, data, refetch } = useQuery(WORKSPACE_BY_ID_QUERY, { variables: { id } })
  const [currentBoard, setCurrentBoard] = useState({})

  useEffect(() => {
    if (loading === false && data) {
      setCurrentBoard(data.workspace[0].boards[0] || {})
    }
  }, [loading, data])

  if (loading) return <></>

  function changeBoard(board: any) {
    setCurrentBoard(board)
  }

  return (
    <section className="grid grid-cols-24 grid-flow-col gap-0 h-full max-h-full">
      <div className="col-span-4 sm:col-span-2 md:col-span-2 xl:col-span-1">
        <div className="w-full h-full bg-gray-900 text-white p-2">
          <nav className="flex flex-col flex-shrink-0 h-full">
            <div className="flex flex-col overflow-y-scroll gap-2 m-0 p-0 max-h-128 overflow-style-hidden">
              {loading || data.workspace[0].boards.map((board: any) => <SideNavButton key={board.id} board={board} onClick={() => changeBoard(board)} />)}
            </div>
            <div className="mt-auto">
              <AddBoardButton workspaceId={id} refetch={refetch} />
              <SettingsButton />
            </div>
          </nav>
        </div>
      </div>
      <div className="col-span-20 sm:col-span-22 md:col-span-22 xl:col-span-23 bg-indigo-500 overflow-x-auto">
        <RenderBoard board={currentBoard} />
      </div>
    </section>
  )
}
