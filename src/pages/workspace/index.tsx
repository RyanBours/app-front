import React from 'react'
import { GetServerSideProps } from 'next'
import { useSession, getSession } from 'next-auth/react'
import { useQuery, useMutation } from '@apollo/client'
import Error from 'next/error'

import Footer from '../../components/Footer'
import Card from '../../components/dashboard/Card'

import MY_WORKSPACE_QUERY from '../../apollo/query/MyWorkspacesQuery'
import SHARED_WORKSPACE_QUERY from '../../apollo/query/SharedWorkspaceQuery'
import CREATE_EMPTY_WORKSPACE from '../../apollo/mutation/CreateEmptyWorkspace'

export function CreateEmptyWorkspaceCard() {
  const [createEmptyWorkspace, { data, loading, error }] = useMutation(CREATE_EMPTY_WORKSPACE)
  if (error) {
    return (
      <p>
        Error :( $
        {error.message}
      </p>
    )
  }
  if (data) {
    const { id } = data.createEmptyWorkspace
    window.location.href = `/workspace/${id}`
  }

  return (
    <div className="py-8 px-8 sm:p-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 2xl:w-1/6 h-96">
      <div className="flex rounded-br-none rounded-lg h-full bg-gray-900 flex-col border-b-8 border-r-8 border-indigo-500">
        <div className="rounded-none transform m-auto h-full w-full my-16 py-16">
          <p className="text-center px-2">
            {loading
              ? <span className="leading-relaxed text-xl md:text-2xl text-white underline cursor-wait">Creating empty workspace...</span>
              : <button type="button" onClick={() => createEmptyWorkspace()} className="leading-relaxed text-xl md:text-2xl text-white underline"><h1>Create Empty Workspace</h1></button>}
          </p>
        </div>
      </div>
    </div>
  )
}

export function WorkspaceCards({ query, variables }: any) {
  const { data, loading, error } = useQuery(query, { variables })
  if (loading) {
    return (
      <h2>
        <svg aria-hidden="true" className="aal_svg" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fillRule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z" /></svg>
        Loading...
      </h2>
    )
  }

  if (error) {
    /* eslint-disable no-console */
    console.error(error)
    return null
  }

  return (
    <>
      {data.workspace.map((workspace: any) => (<Card key={workspace.id} data={workspace} />))}
    </>
  )
}

export default function Workspaces() {
  const { data: session } = useSession()
  if (!session) return <Error statusCode={401} />

  return (
    <>
      <section id="my-workspaces" className="py-4 mx-8">
        <h1 className="sm:text-4xl text-5xl font-medium title-font mb-8 text-gray-900">My Workspaces</h1>
        <div className="flex flex-wrap">
          <CreateEmptyWorkspaceCard />
          <WorkspaceCards query={MY_WORKSPACE_QUERY} variables={{ owner: session!.id }} />
        </div>
      </section>
      <section id="shared-workspaces" className="py-4 mx-8">
        <h1 className="sm:text-4xl text-5xl font-medium title-font mb-8 text-gray-900">Shared Workspaces</h1>
        <div className="flex flex-wrap">
          <WorkspaceCards query={SHARED_WORKSPACE_QUERY} variables={{ userId: session!.id }} />
        </div>
      </section>
      <Footer />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context)

  if (!session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }

  return {
    props: {
      session,
    },
  }
}
