import React from 'react'
import { Disclosure } from '@headlessui/react'

import Footer from '../components/Footer'

function Faq({ question, answer }: { question: string, answer: string }) {
  return (
    <Disclosure>
      {({ open }) => (
        <>
          <Disclosure.Button className="flex justify-between w-full px-4 py-4 m-2 text-lg font-medium text-left bg-gray-800 text-white underline-pink-500">
            <span>{question}</span>
            {open
              ? (
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-pink-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 4v16m8-8H4" />
                </svg>
              )
              : (
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-pink-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20 12H4" />
                </svg>
              )}
          </Disclosure.Button>
          <Disclosure.Panel className="p-4 text-md font-semibold text-white bg-gray-800 border-indigo-500 border-b-4 border-r-4">
            {answer}
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  )
}

export default function privacy() {
  return (
    <>
      <section id="faq" className="py-4 mx-8 pb-8">
        <h1 className="sm:text-4xl text-5xl font-medium title-font mb-2 text-gray-900">Frequently Asked Questions</h1>
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
        <Faq
          question="What is your refund policy?"
          answer="If you're unhappy with your purchase for any reason, email us
                        within 90 days and we'll refund you in full, no questions asked."
        />
      </section>
      <Footer />
    </>
  )
}
