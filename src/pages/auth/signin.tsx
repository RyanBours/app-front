import { signIn } from 'next-auth/react'
import React, { useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'

// TODO: error handling on client side
// TODO: input validation

export default function Signin() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const submitCredentials = (e: any) => {
    e.preventDefault()
    signIn('credentials', {
      redirect: true,
      email,
      password,
      callbackUrl: '/workspace',
    })
  }

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 p-4 h-full">
      <div className="my-auto mx-4">
        <Image priority alt="" width={1166.17} height={805.09} layout="responsive" src="/undraw_authentication.svg" />
      </div>
      <section className="my-auto order-first md:order-none">
        <h1>Sign in</h1>
        <button type="button" onClick={() => signIn('github')} className="bg-gray-900 text-white flex p-2 m-auto mt-4">
          <Image width={32} height={32} src="/GitHub-Mark-Light-32px.png" alt="" />
          <span className="pl-2 m-auto">Sign in, with github</span>
        </button>
        <div className="text-divider mt-4 text-pink-500">OR</div>
        <form>
          <label htmlFor="email" className="block w-full max-w-prose mt-4 mx-auto">
            <span className="text-black font-semibold">Email</span>
            <input
              name="email"
              className="mt-1 block w-full"
              type="email"
              placeholder="Email"
              value={email}
              autoComplete="email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label htmlFor="password" className="block w-full max-w-prose mt-4 mx-auto">
            <span className="text-black font-semibold">Password</span>
            <input
              name="password"
              className="mt-1 block w-full"
              type="password"
              placeholder="Password"
              value={password}
              autoComplete="current-password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <div className="block w-full max-w-prose mx-auto">
            <button onClick={submitCredentials} className="mt-4 px-4 btn btn-pink" type="submit">Signin</button>
            <p>
              Don&apos;t have an account yet,
              <Link href="/auth/signup"><a className="underline-pink-500">Sign up here</a></Link>
            </p>
          </div>
        </form>
      </section>
    </div>
  )
}
