import { useMutation } from '@apollo/client'
import { signIn } from 'next-auth/react'
import React, { useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'

import SIGN_UP_MUTATION from '../../apollo/mutation/SignUp'

// TODO: check for confirm password
// TODO: add error handling on client side
// TODO: input validation
// TODO: check for existing email
// TODO: signup via github

export default function Signup() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const [signUp, { data, loading, error }] = useMutation(SIGN_UP_MUTATION, {
    variables: {
      email,
      password,
      confirmPassword,
    },
  })

  if (loading) {
    return <p>Signing up</p>
  }
  if (error) {
    return <p>Error signing up :(</p>
  }

  if (data) {
    signIn('credentials', {
      redirect: false,
      callbackUrl: '/api/auth/callback/credentials',
      email,
      password,
    })
    return <p>Successfully signed up!</p>
  }

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 p-4 h-full">
      <div className="my-auto mx-4">
        <Image priority alt="" width={889.07556} height={459.37952} layout="responsive" src="/undraw_welcome_cats.svg" />
      </div>
      <section className="my-auto order-first md:order-none">
        <h1>Sign up</h1>

        <button type="button" className="bg-gray-900 text-white flex p-2 m-auto mt-4 ">
          <Image width={32} height={32} src="/GitHub-Mark-Light-32px.png" alt="" />
          <span className="pl-2 m-auto">Signup with github</span>
        </button>
        <div className="text-divider mt-4 text-pink-500">OR</div>
        <form action="">
          <label htmlFor="email" className="block w-full max-w-prose mt-4 mx-auto">
            <span className="text-black font-semibold">Email</span>
            <input
              name="email"
              className="mt-1 block w-full"
              type="email"
              placeholder="Email"
              value={email}
              autoComplete="email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label htmlFor="password" className="block w-full max-w-prose mt-4 mx-auto">
            <span className="text-black font-semibold">Password</span>
            <input
              name="password"
              className="mt-1 block w-full"
              type="password"
              placeholder="Password"
              value={password}
              autoComplete="new-password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <label htmlFor="passwordConfirm" className="block w-full max-w-prose mt-4 mx-auto">
            <span className="text-black font-semibold">Confirm Password</span>
            <input
              name="passwordConfirm"
              className="mt-1 block w-full"
              type="password"
              placeholder="Confirm Password"
              value={confirmPassword}
              autoComplete="new-password"
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </label>
          <div className="block w-full max-w-prose mx-auto">
            <button onClick={() => signUp()} className="mt-4 px-4 btn btn-pink" type="submit">Signup</button>
            <p>
              Already have an account,
              <Link href="/auth/signin"><a className="underline-pink-500">Sign in here</a></Link>
            </p>
          </div>
        </form>
      </section>
    </div>
  )
}
