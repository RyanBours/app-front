import React from 'react'
import Footer from '../components/Footer'

export default function privacy() {
  return (
    <>
      <section id="privacy-statement" className="py-4 mx-8 pb-8">
        <h1 className="sm:text-4xl text-5xl font-medium title-font mb-2 text-gray-900">Privacy statement</h1>
        <h2>1. identity of</h2>
        <h2>2. What information do we collect?</h2>
        <h2>3. What do we use your information for?</h2>
        <h2>4. Legal basis</h2>
        <h2>5. How do we protect your information?</h2>
        <h2>6. How we use cookies</h2>
        <h2>7. Do we disclose any information to outside parties?</h2>
        <h2>8. Third party links</h2>
        <h2>9. Where do we store the information?</h2>
        <h2>10. Access, data portability, migration, and transfer back assistance</h2>
        <h2>11. Request for rectification, restriction or erasure of the personal data</h2>
        <h2>12. Data retention</h2>
        <h2>13. Accountability</h2>
        <h2>14. Cooperation</h2>
        <h2>15. Terms of Service</h2>
        <h2>16. Your consent</h2>
        <h2>17. Changes to our Privacy Policy</h2>
        <h2>18. Complaint</h2>
      </section>
      <Footer />
    </>
  )
}
