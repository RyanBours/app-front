/* eslint-disable react/jsx-props-no-spreading */
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import { SessionProvider } from 'next-auth/react'
import client from '../apollo/client'
import BaseLayout from '../components/BaseLayout'

function App({ Component, pageProps }: AppProps) {
  return (
    <SessionProvider session={pageProps.session}>
      <ApolloProvider client={client}>
        <BaseLayout>
          <Component {...pageProps} />
        </BaseLayout>
      </ApolloProvider>
    </SessionProvider>
  )
}
export default App
