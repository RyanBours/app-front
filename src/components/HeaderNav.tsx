import React, { Fragment } from 'react'
import Link from 'next/link'
import { Menu, Transition, Popover } from '@headlessui/react'
import { useSession, signIn, signOut } from 'next-auth/react'

// TODO: logo redirects to dashboard when user has session

function AuthenticatedMenuItems() {
  return (
    <>
      <Menu.Item>
        <a
          className="group flex items-center w-full px-2 py-2 text-sm"
          href="/account-settings"
        >
          Account settings
        </a>
      </Menu.Item>
      <div className="group flex items-center w-full px-2 py-2 text-sm">
        <Menu.Item>
          <button type="button" className="text-white underline-pink-500" onClick={() => signOut({ callbackUrl: '/' })}>SignOut</button>
        </Menu.Item>
      </div>
    </>
  )
}

function AuthenticatedMenu() {
  return (
    <Menu>
      <Menu.Button>
        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 w-56 origin-top-right bg-gray-900 text-white divide-y divide-gray-100 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none border-indigo-500 border-b-4 border-r-4 m-2">
          <AuthenticatedMenuItems />
        </Menu.Items>
      </Transition>
    </Menu>
  )
}

function MobileNav({ status }: { status: string }) {
  return (
    <Popover className="relative">
      {({ open }) => (
        <>
          <Popover.Button>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
            </svg>
          </Popover.Button>
          <Popover.Overlay
            className={`${open ? 'opacity-40 fixed inset-0 mix-blend-overlay ' : 'opacity-0'} bg-black`}
          />
          <Transition
            as={Fragment}
            enter="duration-100"
            enterFrom="translate-x-full"
            enterTo="translate-x-0"
            leave=""
            leaveFrom=""
            leaveTo=""
          >
            <Popover.Panel className="fixed right-0 top-0 z-50" as="nav">
              <div className="w-64 h-screen bg-gray-800">
                <div className="p-4 bg-gray-900 flex justify-between">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                  </svg>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                  </svg>
                </div>
                <div className="">
                  <div className="md:hidden">
                    <Link href="/workspace" passHref>
                      <a className="flex items-center mt-5 py-2 px-8 text-gray-400 border-r-4 border-gray-800 hover:bg-gray-700 hover:text-gray-100 hover:border-pink-500" href="#">
                        <svg className="h-5 w-5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M19 11H5M19 11C20.1046 11 21 11.8954 21 13V19C21 20.1046 20.1046 21 19 21H5C3.89543 21 3 20.1046 3 19V13C3 11.8954 3.89543 11 5 11M19 11V9C19 7.89543 18.1046 7 17 7M5 11V9C5 7.89543 5.89543 7 7 7M7 7V5C7 3.89543 7.89543 3 9 3H15C16.1046 3 17 3.89543 17 5V7M7 7H17" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <span className="mx-4 font-medium">Workspaces</span>
                      </a>
                    </Link>
                  </div>
                  <a className="flex items-center mt-5 py-2 px-8 text-gray-400 border-r-4 border-gray-800 hover:bg-gray-700 hover:text-gray-100 hover:border-pink-500" href="#">
                    <svg className="h-5 w-5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M10.3246 4.31731C10.751 2.5609 13.249 2.5609 13.6754 4.31731C13.9508 5.45193 15.2507 5.99038 16.2478 5.38285C17.7913 4.44239 19.5576 6.2087 18.6172 7.75218C18.0096 8.74925 18.5481 10.0492 19.6827 10.3246C21.4391 10.751 21.4391 13.249 19.6827 13.6754C18.5481 13.9508 18.0096 15.2507 18.6172 16.2478C19.5576 17.7913 17.7913 19.5576 16.2478 18.6172C15.2507 18.0096 13.9508 18.5481 13.6754 19.6827C13.249 21.4391 10.751 21.4391 10.3246 19.6827C10.0492 18.5481 8.74926 18.0096 7.75219 18.6172C6.2087 19.5576 4.44239 17.7913 5.38285 16.2478C5.99038 15.2507 5.45193 13.9508 4.31731 13.6754C2.5609 13.249 2.5609 10.751 4.31731 10.3246C5.45193 10.0492 5.99037 8.74926 5.38285 7.75218C4.44239 6.2087 6.2087 4.44239 7.75219 5.38285C8.74926 5.99037 10.0492 5.45193 10.3246 4.31731Z" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                      <path d="M15 12C15 13.6569 13.6569 15 12 15C10.3431 15 9 13.6569 9 12C9 10.3431 10.3431 9 12 9C13.6569 9 15 10.3431 15 12Z" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                    <span className="mx-4 font-medium">Settings</span>
                  </a>
                </div>
                <div className="absolute bottom-0 bg-gray-900 w-full">
                  <a className="flex items-center py-2 px-8 text-gray-600 hover:text-gray-500" href="#">
                    {
                      status === 'authenticated'
                        ? <button type="button" className="text-white underline-pink-500" onClick={() => signOut({ callbackUrl: '/' })}>SignOut</button>
                        : <button type="button" className="text-white underline-pink-500" onClick={() => signIn()}>SignIn</button>
                    }
                  </a>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  )
}

export default function HeaderNav() {
  const { status } = useSession()
  return (
    <header className="row-span-1 col-span-24">
      <nav className="flex justify-between bg-gray-900 text-white h-full w-full">
        <div className="px-5 xl:px-12 py-6 flex w-full items-center">
          <Link href="/">
            <a className="text-3xl font-bold font-heading">
              Logo Here.
            </a>
          </Link>

          <ul className="hidden md:flex px-4 ml-16 mr-auto font-semibold font-heading space-x-12">
            <li><Link href="/"><a className="hover:text-gray-200">Home</a></Link></li>
            <li><Link href="/workspace"><a className="hover:text-gray-200">Workspaces</a></Link></li>
          </ul>

          <div className="hidden xl:flex items-center space-x-5">
            <div>
              {
                (status === 'authenticated')
                  ? <AuthenticatedMenu />
                  : <button type="button" className="text-white underline-pink-500" onClick={() => signIn()}>SignIn</button>
              }
            </div>
          </div>
        </div>

        {/* Mobile Navbar */}
        <div className="self-center mr-12 xl:hidden">
          <MobileNav status={status} />
        </div>
      </nav>
    </header>
  )
}
