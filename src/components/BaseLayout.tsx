import React from 'react'
import HeaderNav from './HeaderNav'

export default function GuestLayout({ children }: any) {
  return (
    <main className="grid grid-rows-12 grid-cols-24 grid-flow-col gap-0 h-full">
      <HeaderNav />
      <div className="row-span-11 col-span-24">
        {children}
      </div>
    </main>
  )
}
