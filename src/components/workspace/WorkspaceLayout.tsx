import React from 'react'
import SideNav from './sidenav/SideNav'

export default function WorkspaceLayout({ children, workspace, setCurrentBoard }: any) {
  return (
    <section className="grid grid-cols-24 grid-flow-col gap-0 h-full max-h-full">
      <div className="col-span-4 sm:col-span-2 md:col-span-2 xl:col-span-1">
        <SideNav workspaceId={workspace.id} setCurrentBoard={setCurrentBoard} />
      </div>
      <div className="col-span-20 sm:col-span-22 md:col-span-22 xl:col-span-23 bg-indigo-500 overflow-x-auto">
        {children}
      </div>
    </section>
  )
}
