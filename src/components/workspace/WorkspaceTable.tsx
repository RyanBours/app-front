import React from 'react'

function WorkspaceRow() {
  return (
    <tr className="border-b border-gray-200 hover:bg-gray-700 bg-gray-800 text-white hover:border-b-pink-500">
      <td className="py-3 px-6 text-left whitespace-nowrap">
        <div className="flex items-center">
          <span className="font-medium">New Project</span>
        </div>
      </td>
      <td className="py-3 px-6 text-center">
        <div className="flex items-center justify-center">
          <img className="w-6 h-6 rounded-full border-gray-200 border transform hover:scale-125" src="https://randomuser.me/api/portraits/men/1.jpg" />
          <img className="w-6 h-6 rounded-full border-gray-200 border -m-1 transform hover:scale-125" src="https://randomuser.me/api/portraits/women/2.jpg" />
          <img className="w-6 h-6 rounded-full border-gray-200 border -m-1 transform hover:scale-125" src="https://randomuser.me/api/portraits/men/3.jpg" />
        </div>
      </td>
      <td className="py-3 px-6 text-center">
        <span className="font-medium">New Project</span>
      </td>
      <td className="py-3 px-6 text-center">
        <div className="flex item-center justify-end">
          <div className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
            </svg>
          </div>
          <div className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg>
          </div>
        </div>
      </td>
    </tr>
  )
}

export default function WorkspaceTable() {
  return (
    <table className="min-w-max w-full table-auto">
      <thead>
        <tr className="bg-gray-900 text-white uppercase text-sm leading-normal">
          <th className="py-3 px-6 text-left">Project</th>
          <th className="py-3 px-6 text-center">Collaborators</th>
          <th className="py-3 px-6 text-center">Last activity</th>
          <th className="py-3 px-6 text-right">Actions</th>
        </tr>
      </thead>
      <tbody className="text-gray-600 text-sm font-light">
        <WorkspaceRow />
        <WorkspaceRow />
        <WorkspaceRow />
        <WorkspaceRow />
      </tbody>
    </table>
  )
}
