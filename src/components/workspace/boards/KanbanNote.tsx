/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'

function NoteForm({ note, setEdit }: any) {
  const { content } = note
  const [contentState, setContentState] = useState(content)
  const saveHandler = () => {
    // TODO: save note content
    setEdit(false)
  }
  return (
    <>
      <textarea className="mt-1" cols={23} rows={5} value={contentState} onChange={(e) => { setContentState(e.target.value) }} />
      <button type="button" onClick={saveHandler} className="bg-pink-500 text-black mt-1 px-2 py-1 rounded font-medium hover:bg-pink-600">save</button>
    </>
  )
}

export default function KanbanNote({ note, index }: any) {
  const { content, id } = note
  const [edit, setEdit] = useState(false)
  return (
    <Draggable key={id} draggableId={id} index={index}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={{
            userSelect: 'none',
            margin: '0 0 8px 0',
            minHeight: '50px',
            color: 'white',
            ...provided.draggableProps.style,
          }}
        >
          <div className="bg-white shadow rounded-lg p-2 m-2 min-h-24">
            <h3 className="text-base font-normal text-black mb-0">
              <div className="w-full flex justify-end">
                <button type="button" onClick={() => setEdit(!edit)} className="">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                  </svg>
                </button>
              </div>
              {!edit ? content : <NoteForm note={note} setEdit={setEdit} />}
            </h3>
          </div>
        </div>
      )}
    </Draggable>
  )
}
