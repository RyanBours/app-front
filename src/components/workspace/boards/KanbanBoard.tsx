/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react'
import {
  DragDropContext,
} from 'react-beautiful-dnd'
import client from '../../../apollo/client'
import CREATE_LANE from '../../../apollo/mutation/CreateLane'
import CREATE_NOTE from '../../../apollo/mutation/CreateNote'
import UPDATE_NOTE_LANE from '../../../apollo/mutation/UpdateNoteLane'
import UPDATE_NOTE_ORDER from '../../../apollo/mutation/UpdateNoteOrder'
import KanbanLane from './KanbanLane'

const onDragEnd = (result: any, lanes: LaneProp[], setLanes: any) => {
  if (!result.destination) return
  const { source, destination } = result

  if (source.droppableId !== destination.droppableId) {
    const sourceLaneIndex = lanes.findIndex((lane: LaneProp) => lane.id === source.droppableId)
    const destLaneIndex = lanes.findIndex((lane: LaneProp) => lane.id === destination.droppableId)

    setLanes((oldLanes: LaneProp[]) => {
      const newLanes = JSON.parse(JSON.stringify(oldLanes))
      const [removed] = newLanes[sourceLaneIndex].notes.splice(source.index, 1)
      newLanes[destLaneIndex].notes.splice(destination.index, 0, removed)

      newLanes[sourceLaneIndex].notes.forEach((note: any, index: number) => {
        client.mutate({
          mutation: UPDATE_NOTE_ORDER,
          variables: {
            noteId: note.id,
            order: index,
          },
        })
      })

      newLanes[destLaneIndex].notes.forEach((note: any, index: number) => {
        client.mutate({
          mutation: UPDATE_NOTE_ORDER,
          variables: {
            noteId: note.id,
            order: index,
          },
        })
      })

      client.mutate({
        mutation: UPDATE_NOTE_LANE,
        variables: {
          noteId: removed.id,
          laneId: newLanes[destLaneIndex].id,
        },
      })

      return newLanes
    })
  } else {
    // movement within the same lane
    const targetLaneIndex = lanes.findIndex((lane: LaneProp) => lane.id === source.droppableId)

    setLanes((oldLanes: LaneProp[]) => {
      const newLanes = JSON.parse(JSON.stringify(oldLanes))
      const [removed] = newLanes[targetLaneIndex].notes.splice(source.index, 1)
      newLanes[targetLaneIndex].notes.splice(destination.index, 0, removed)

      newLanes[targetLaneIndex].notes.forEach((note: any, index: number) => {
        client.mutate({
          mutation: UPDATE_NOTE_ORDER,
          variables: {
            noteId: note.id,
            order: index,
          },
        })
      })

      return newLanes
    })
  }
}

export default function KanbanBoard({ board }: any) {
  const [lanes, setLanes] = useState<any[]>([])

  useEffect(() => {
    setLanes(board.lanes)
  }, [board.lanes])

  const addLaneHandler = async () => {
    const lane = await client.mutate({
      mutation: CREATE_LANE,
      variables: {
        board: board.id,
        order: lanes.length,
        title: 'New Lane',
      },
    })
    const newLanes = JSON.parse(JSON.stringify(lanes))
    newLanes.push(lane.data.createKanbanLane)
    setLanes(newLanes)
  }

  const addNoteHandler = async (lane: LaneProp) => {
    const { data } = await client.mutate({
      mutation: CREATE_NOTE,
      variables: {
        lane: lane.id,
        content: 'New Note',
        order: lane.notes.length,
      },
    })

    const newLanes = JSON.parse(JSON.stringify(lanes))
    const newLane = newLanes.find((l: LaneProp) => l.id === lane.id)
    newLane.notes.push(data.createKanbanNote)
    setLanes(newLanes)
  }

  const kanbanLanes = lanes.map((lane: any) => <KanbanLane lane={lane} key={lane.id} createNoteHandler={() => addNoteHandler(lane)} />)

  return (
    <div className="mx-2">
      <DragDropContext onDragEnd={(result) => onDragEnd(result, lanes, setLanes)}>
        <div className="flex flex-row items-stretch h-100">
          {kanbanLanes}
        </div>
        <button type="button" onClick={addLaneHandler} className="btn btn-pink">Add Lane</button>
      </DragDropContext>
    </div>
  )
}
