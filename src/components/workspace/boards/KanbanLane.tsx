/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { Droppable } from 'react-beautiful-dnd'

import KanbanNote from './KanbanNote'

export default function KanbanLane({ lane, createNoteHandler }: any) {
  const { title, id } = lane
  return (
    <div className="p-1">
      <h2 className="text-xl w-64 mx-1">{title}</h2>
      <Droppable droppableId={id} key={id}>
        {(provided, snapshot) => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={{
              background: snapshot.isDraggingOver
                ? 'lightblue'
                : '',
              padding: 4,
              width: 250,
              minHeight: 500,
            }}
          >
            <button type="button" onClick={createNoteHandler} className="ml-1 btn btn-pink">Add Note</button>
            {lane.notes.map((note: any, index: number) => <KanbanNote note={note} key={note.id} index={index} />)}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </div>
  )
}
