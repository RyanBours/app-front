import React from 'react'

function AddBoardIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" className="rounded-sm bg-gray-700 text-white w-10 h-w-10 m-2 inline" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
    </svg>
  )
}

export default function EmptyWorkspace() {
  return (
    <div className="grid grid-cols-none md:grid-cols-2 place-content-center w-full h-full">
      <div className="font-mono py-16 pr-2 border-black border-r-2">
        <p className="text-2xl ml-64 text-right">
          Press the
          <AddBoardIcon />
          button on the left to add a board
        </p>
      </div>
      <div className="font-mono py-16 pl-2 border-black border-l-2" />
    </div>
  )
}
