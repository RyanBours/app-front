import { useQuery } from '@apollo/client'

import React from 'react'

import BOARDS_BY_WORKSPACE_QUERY from '../../../apollo/query/BoardsByWorkspace'
import AddBoardButton from './AddBoardButton'

import SettingsButton from './SettingsButton'
import SideNavButton from './SideNavButton'

export default function SideNav({ workspaceId, setCurrentBoard }: any) {
  const { loading, data } = useQuery(BOARDS_BY_WORKSPACE_QUERY, {
    variables: { workspaceId },
  })

  return (
    <div className="w-full h-full bg-gray-900 text-white p-2">
      <nav className="flex flex-col flex-shrink-0 h-full">
        <div className="flex flex-col overflow-y-scroll gap-2 m-0 p-0 max-h-128 overflow-style-hidden">
          {loading ? (
            <></>
          ) : (
            data.boards.map((board: any) => (
              <SideNavButton
                key={board.id}
                board={board}
                setCurrentBoard={setCurrentBoard}
              />
            ))
          )}
        </div>
        <div className="mt-auto">
          <AddBoardButton workspaceId={workspaceId} />
          <SettingsButton />
        </div>
      </nav>
    </div>
  )
}
