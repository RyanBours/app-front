import { Popover, RadioGroup } from '@headlessui/react'
import Tippy from '@tippyjs/react'
import { useState } from 'react'
import CREATE_KABAN_BOARD from '../../../apollo/mutation/CreateKabanBoard'
import client from '../../../apollo/client'

function BoardOption({
  children,
  value,
  label,
}: {
  children?: undefined | React.ReactNode,
  value: string;
  label: string;
}) {
  return (
    <RadioGroup.Option value={value}>
      {({ checked }) => (
        <div
          className={
            checked
              ? 'text-center rounded-md rounded-br-none border-pink-500 border-b-4 border-r-4 bg-gray-900 text-white'
              : 'text-center rounded-md rounded-br-none border-gray-400 hover:border-indigo-500 border-b-4 border-r-4 bg-gray-900 text-white'
          }
        >
          {children ?? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
              />
            </svg>
          )}
          <p className="p-2 bg-gray-900 text-white rounded-b-md">{label}</p>
        </div>
      )}
    </RadioGroup.Option>
  )
}

function BoardSelection({ board, setBoard, boardTypes }: any) {
  return (
    <RadioGroup value={board} onChange={setBoard} className="text-black">
      <div className="grid grid-cols-3 gap-4">
        {boardTypes.map(({ type, label, id }: any) => (
          <BoardOption key={id} value={type} label={label} />
        ))}
      </div>
    </RadioGroup>
  )
}

export default function AddBoardButton({ workspaceId, refetch }: any) {
  const boardTypes = [
    { type: 'Kanban', label: 'Kanban', id: 0 },
    // { type: 'Freehand', label: 'Freehand' },
    // { type: 'Calendar', label: 'Calendar' },
    // { type: 'FreeForm', label: 'FreeForm' },
    // { type: 'Placeholder', label: 'placeholder' },
  ]
  const [board, setBoard] = useState(boardTypes[0].type)
  const [boardTitle, setBoardTitle] = useState('')

  const createBoardHandler = async () => {
    let mutation
    switch (board) {
      case 'Kanban':
        mutation = CREATE_KABAN_BOARD
        break
      default:
        return
    }

    await client.mutate({
      mutation,
      variables: {
        workspace: workspaceId,
        title: boardTitle || 'New Board',
      },
    })

    setBoardTitle('')
    refetch()
  }

  return (
    <Popover>
      {({ open, close }) => (
        <>
          <Popover.Button className="w-full h-full m-0 p-0 ">
            <Tippy placement="right" content="Add Board">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="rounded-sm bg-gray-700 text-white hover:text-gray-200"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z"
                />
              </svg>
            </Tippy>
          </Popover.Button>
          <Popover.Overlay
            className={`${open ? 'opacity-50 fixed inset-0' : 'opacity-0'} bg-black`}
          />
          <Popover.Panel className="fixed inset-0 flex items-center justify-center">
            <div className="flex flex-col w-11/12 sm:w-5/6 lg:w-1/2 max-w-2xl mx-auto ">
              <div className="flex flex-row justify-between p-6 bg-gray-900 border-r-4 border-pink-500">
                <p className="font-semibold text-white">Select a board type</p>
                <svg
                  onClick={() => close()}
                  className="w-6 h-6 hover:text-gray-200"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </div>
              <div className="flex flex-col px-6 py-5 bg-gray-50 border-r-4 border-pink-500">
                <BoardSelection
                  boardTypes={boardTypes}
                  board={board}
                  setBoard={setBoard}
                />
              </div>
              <div className="flex flex-row items-center justify-between p-5 bg-gray-900 border-b-4 border-r-4 border-pink-500">
                <button
                  type="button"
                  className="px-4 py-2 text-white font-semibold bg-pink-500 rounded"
                  onClick={async () => {
                    createBoardHandler()
                    close()
                  }}
                >
                  Add Board
                </button>
                <input
                  className="text-black"
                  type="text"
                  onChange={(e) => setBoardTitle(e.target.value)}
                  value={boardTitle}
                  placeholder="Your Board Title"
                />
              </div>
            </div>
          </Popover.Panel>
        </>
      )}
    </Popover>
  )
}
