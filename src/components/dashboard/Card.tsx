import React from 'react'

export default function Card({ data }: any) {
  return (
    <div className="px-8 py-8 sm:p-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 2xl:w-1/6 h-96">
      <div className="flex rounded-lg h-full bg-gray-900 p-8 flex-col rounded-br-none border-b-8 border-r-8 border-indigo-500">
        <div className="flex items-center mb-3">
          <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-none skew-x-12 bg-pink-500 text-white flex-shrink-0">
            <span className="-skew-x-12">
              <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} className="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2" />
                <circle cx="12" cy="7" r="4" />
              </svg>
            </span>
          </div>
          <h2 className="text-white text-lg title-font font-medium">{data.title}</h2>
        </div>
        <p className="leading-relaxed text-base text-white">Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.</p>
        <div className="mt-auto">
          <div className="mt-3 bg-pink-500 skew-x-12">
            <a href={`/workspace/${data.id}`} className="m-2 -skew-x-12 text-white hover:text-gray-900 inline-flex items-center">
              Open Workspace
              <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7" />
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
