import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client'

const client = new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: createHttpLink({
        uri: process.env.NEXT_PUBLIC_GATEWAY_URL,
        credentials: 'include'
    }),
    cache: new InMemoryCache(),
    name: 'app-front',
})

export default client
