import { gql } from '@apollo/client'

// TODO: limit collaborators
const WORKSPACE_BY_ID_QUERY = gql`
    query queryWorkspaceById($id: String!) {
        workspace(id: $id) {
            id
            title
            collaborators { 
                id
                # name
                email
            }
            boards { 
                id
                title
                ... on Kanban {
                    id
                    lanes { 
                        title
                        notes { 
                            content
                        }
                    }
                }
            }
        }
    }
`

export default WORKSPACE_BY_ID_QUERY
