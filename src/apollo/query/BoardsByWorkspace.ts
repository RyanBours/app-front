import { gql } from '@apollo/client'

const BOARDS_BY_WORKSPACE_QUERY = gql`
    query queryBoardsByWorkspace($workspaceId: ID!) {
        boards(workspace: $workspaceId) {
            id
            title
            type
        }
    }
`
export default BOARDS_BY_WORKSPACE_QUERY
