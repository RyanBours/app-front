import { gql } from '@apollo/client'

const SHARED_WORKSPACE_QUERY = gql`
query querySharedWorkspaces($userId: String) {
    workspace(collaborators: [$userId]) {
        id
        title
    }
}`

export default SHARED_WORKSPACE_QUERY
