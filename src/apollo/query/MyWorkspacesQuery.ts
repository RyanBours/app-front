import { gql } from '@apollo/client'

const MY_WORKSPACE_QUERY = gql`
query queryWorkspaces($owner: String) {
    workspace(owner: $owner) {
        id
        title
    }     
}`

export default MY_WORKSPACE_QUERY
