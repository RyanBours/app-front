import { gql } from '@apollo/client'

const WORKSPACE_IDS_QUERY = gql`
query queryWorkspaces {
    workspace {
        id
    }
}`

export default WORKSPACE_IDS_QUERY
