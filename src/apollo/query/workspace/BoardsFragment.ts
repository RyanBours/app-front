import { gql } from '@apollo/client'

const BOARDS_FRAGMENT = gql`
    fragment BoardsFragment on Workspace {
        id
        title
    }
`

export default BOARDS_FRAGMENT
