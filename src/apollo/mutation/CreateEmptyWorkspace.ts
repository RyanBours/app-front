import { gql } from '@apollo/client'

const CREATE_EMPTY_WORKSPACE = gql`
    mutation mutateCreateEmptyWorkspace {
        createEmptyWorkspace  { 
            id
            title
            createdAt
        }
    }
`

export default CREATE_EMPTY_WORKSPACE
