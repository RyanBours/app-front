import { gql } from '@apollo/client'

const SIGN_UP_MUTATION = gql`
    mutation SignUp($email: String, $password: String) {
        signUp(email: $email, password: $password) {
            id
        }
    }
`

export default SIGN_UP_MUTATION
