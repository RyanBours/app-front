import { gql } from '@apollo/client'

const UPDATE_NOTE_LANE = gql`
    mutation UpdateNoteLane($laneId: String!, $noteId: String!) {
        updateNoteLane(laneId: $laneId, noteId: $noteId) {
            id
        }
    }
`

export default UPDATE_NOTE_LANE
