import { gql } from '@apollo/client'

const CREATE_KABAN_BOARD = gql`
    mutation CreateKanbanBoard($title: String!, $workspace: String!) {
        createKanbanBoard(title: $title, workspace: $workspace) {
            id
        }
    }
`

export default CREATE_KABAN_BOARD
