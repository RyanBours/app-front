import { gql } from '@apollo/client'

const CREATE_LANE = gql`
    mutation mutateCreateKanbanLane($board: String!, $order: Int!, $title: String!) {
        createKanbanLane(board: $board, order: $order, title: $title) {
            id
            title
            order
            notes { 
                id
                content
                order
            }
        }
    }
`

export default CREATE_LANE
