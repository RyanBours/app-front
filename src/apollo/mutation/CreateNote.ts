import { gql } from '@apollo/client'

const CREATE_NOTE = gql`
    mutation mutateCreateKanbanLane($lane: String!, $order: Int!, $content: String!) {
        createKanbanNote(lane: $lane, order: $order, content: $content) {
            id
            content
            order
        }
    }
`

export default CREATE_NOTE
