import { gql } from '@apollo/client'

const UPDATE_NOTE_ORDER = gql`
    mutation UpdateNoteLane($order: Int!, $noteId: String!) {
        updateNoteOrder(noteId: $noteId, order: $order) {
            id
        }
    }
`

export default UPDATE_NOTE_ORDER
