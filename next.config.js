/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: false, // disabled because of react-beautiful-dnd
  swcMinify: true,
  images: {
    domains: ['via.placeholder.com'],
  },
}
