describe('sign in page', () => {
    it('successfully loads', () => {
        cy.visit('/auth/signin')
    })

    it('contains sign in form', () => {
        cy.visit('/auth/signin')
        cy.get('input[type="email"][autocomplete="email"]').should('be.visible')
        cy.get('input[type="password"][autocomplete="current-password"]').should('be.visible')
        cy.get('button[type="submit"]').should('be.visible')
    })

    it('container link to signup page', () => {
        cy.visit('/auth/signin')
        cy.get('a[href="/auth/signup"]').should('be.visible').click()
        cy.url().should('include', '/auth/signup')
    })
})