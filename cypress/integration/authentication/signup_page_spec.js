describe('sign up page', () => {
    it('successfully loads', () => {
        cy.visit('/auth/signup')
    })

    it('contains sign up form', () => {
        cy.visit('/auth/signup')
        cy.get('input[type="email"][autocomplete="email"]').should('be.visible')
        cy.get('input[type="password"][autocomplete="new-password"]').should('be.visible')
        cy.get('input[type="password"][autocomplete="new-password"]').should('be.visible')
        cy.get('button[type="submit"]').should('be.visible')
    })

    it('container link to signin page', () => {
        cy.visit('/auth/signup')
        cy.get('a[href="/auth/signin"]').should('be.visible').click()
        cy.url().should('include', '/auth/signin')
    })
})