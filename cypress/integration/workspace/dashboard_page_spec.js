describe('sign up page', () => {
    it('redirect when not authenticated', () => {
        cy.visit('/workspace')
        cy.url().should(
            'be.equal',
            `${Cypress.config("baseUrl")}/auth/signin`
        )
    })
})