/* eslint-disable no-unused-vars */
namespace NodeJS {
    interface ProcessEnv extends NodeJS.ProcessEnv {
        NEXT_PUBLIC_GATEWAY_URL: string;
    }
}
