const colors = require('tailwindcss/colors')

module.exports = {
    mode: 'jit',
    purge: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
    darkMode: 'media',
    theme: {
        extend: {
            screens: {
                '2lg': '1600px',
            },
            spacing: {
                '128': '32rem'
            },
            gridTemplateRows: {
                '12': 'repeat(12, minmax(0, 1fr))',
            },
            gridRow: {
                'span-10': 'span 10 / span 12',
                'span-11': 'span 11 / span 12',
            },
            gridTemplateColumns: {
                '24': 'repeat(24, minmax(0, 1fr))',
            },
            gridColumn: {
                'span-20': 'span 20 / span 24',
                'span-21': 'span 21 / span 24',
                'span-22': 'span 22 / span 24',
                'span-23': 'span 23 / span 24',
                'span-24': 'span 24 / span 24'
            }
        },
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            gray: colors.trueGray,
            indigo: colors.indigo,
            red: colors.rose,
            yellow: colors.amber,
            pink: colors.pink,
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
    ],
}